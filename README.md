# BrainMapper

This project will aim to address one of the big challenges in imaging-neuroscience: that of how a brain’s functional connectivity, represented by resting-state maps, can be predicted from structural connectivity information obtained from dw-MRI.

## References
In the creation of this code, material was used from the following papers. The general structure of the code and file organisation was inspired from and follows that of the QuickNAT network described in these papers. The original code can be accessed on [GitHub](https://github.com/ai-med/quickNAT_pytorch)

```
@article{roy2019quicknat,
  title={QuickNAT: A fully convolutional network for quick and accurate segmentation of neuroanatomy},
  author={Roy, Abhijit Guha and Conjeti, Sailesh and Navab, Nassir and Wachinger, Christian and Alzheimer's Disease Neuroimaging Initiative and others},
  journal={NeuroImage},
  volume={186},
  pages={713--727},
  year={2019},
  publisher={Elsevier}
}

@article{roy2019bayesian,
  title={Bayesian QuickNAT: Model uncertainty in deep whole-brain segmentation for structure-wise quality control},
  author={Roy, Abhijit Guha and Conjeti, Sailesh and Navab, Nassir and Wachinger, Christian and Alzheimer's Disease Neuroimaging Initiative and others},
  journal={NeuroImage},
  volume={195},
  pages={11--22},
  year={2019},
  publisher={Elsevier}
}
```

