import utils.data_utils as du
import unittest
from unittest import mock

class test_directoryreader(unittest.TestCase):

    def test_return_type(self):
        folder_location = "../well/win-biobank/projects/imaging/data/data3/subjectsAll/"
        print(du.directory_reader(folder_location))
        self.assertEqual(type(du.directory_reader(folder_location)), list)

    def test_return(self):
        folder_location = "../well/win-biobank/projects/imaging/data/data3/subjectsAll/"
        self.assertIsNotNone(du.directory_reader(folder_location))


    def test_return_type_element(self):
        folder_location = "../well/win-biobank/projects/imaging/data/data3/subjectsAll/"
        self.assertEqual(type(du.directory_reader(folder_location)[0]), str)

class test_dataTestTrainValidationSplit(unittest.TestCase):

    def test_functionRun(self):
        folder_location = "../well/win-biobank/projects/imaging/data/data3/subjectsAll/"
        self.assertIsNone(du.data_test_train_validation_split(folder_location, 90, 5))

class test_updateShufflingFlag(unittest.TestCase):

    def test_functionRun(self):
        file_name = "settings.ini"
        self.assertIsNone(du.update_shuffling_flag(file_name))

