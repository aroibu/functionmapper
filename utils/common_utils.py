""" Commom utilities

Description:

    This file contains several functions which can be useful througout the project.

Usage:

    To use content from this folder, import the functions and instantiate them as you wish to use them:

        from utils.common_utils import function_name

"""

import os

def create_folder(path):
    """Folder Creator

    A function which creates a folder at a given path if one does not exist

    Args:
        path (str): destination to check for folder existance
    """

    if not os.path.exists(path):
        os.mkdir(path)