""" Database Generator

Description:

    This file contains the required fuctions for generating the databases required for training the network.
    This file is designed to be a standalone package, intendead to be run separately from the main network.

Usage:

    To use content from this folder, import the functions and instantiate them as you wish to use them:

        from utils.hdf5_generator import function_name

"""

import os
import h5py
import numpy as np
import data_utils as data_utils
import preprocessor as preprocessor
from settings import Settings
from common_utils import create_folder


def convert_hdf5(data_parameters, file_information):
    # First, we split the data:
    if data_parameters['data_split_flag'] == True:
        print('Data is shuffling... This could take a few minutes!')

        if data_parameters['use_data_file'] == True:
            train_subjects, validation_subjects = preprocessor.data_preparation(data_parameters['data_folder_name'],
                                                                            data_parameters['test_percentage'],
                                                                            data_parameters['subject_number'],
                                                                            data_directory=data_parameters['data_directory'],
                                                                            train_inputs=data_parameters['train_data_file'],
                                                                            train_targets=data_parameters['train_output_targets'],
                                                                            rsfMRI_mean_mask_path=data_parameters['rsfmri_mean_mask_path'],
                                                                            dMRI_mean_mask_path=data_parameters['dmri_mean_mask_path'],
                                                                            data_file=data_parameters['data_file'],
                                                                            )
        else:
            train_subjects, validation_subjects = preprocessor.data_preparation(data_parameters['data_folder_name'],
                                                                            data_parameters['test_percentage'],
                                                                            data_parameters['subject_number'],
                                                                            data_directory=data_parameters['data_directory'],
                                                                            train_inputs=data_parameters['train_data_file'],
                                                                            train_targets=data_parameters['train_output_targets'],
                                                                            rsfMRI_mean_mask_path=data_parameters['rsfmri_mean_mask_path'],
                                                                            dMRI_mean_mask_path=data_parameters['dmri_mean_mask_path'],
                                                                            )
        preprocessor.update_shuffling_flag('utils/hdf5_settings.ini')
        print('Data shuffling... Complete!')

    elif data_parameters['train_test_file_read_flag'] == True:

        # Read the subjects from the files!

        train_subjects =  data_utils.load_subjects_from_path(data_directory=data_parameters['data_directory'],
                                                            data_list=data_parameters['train_list'])
        validation_subjects = data_utils.load_subjects_from_path(data_directory=data_parameters['data_directory'],
                                                                data_list=data_parameters['validation_list'])
    
    else:
        raise ValueError('Either a split flag, or a read-from-file flag must be provided as True')

    # Then, we have to read the various test and train data, process them and write them to H5
    # First, let's do this for the training data

    print('-> Processing training data:')

    train_dMRI, train_rsfMRI = preprocessor.load_datasets(subjects = train_subjects, 
                                                        data_directory = data_parameters['data_directory'], 
                                                        input_file = data_parameters['train_data_file'], 
                                                        output_target = data_parameters['train_output_targets'], 
                                                        mean_regression_flag = data_parameters['mean_regression_flag'], 
                                                        mean_regression_all_flag = data_parameters['mean_regression_all_flag'], 
                                                        regression_weights_path = data_parameters['regression_weights_path'],
                                                        dMRI_mean_mask_path = data_parameters['dmri_mean_mask_path'], 
                                                        rsfMRI_mean_mask_path = data_parameters['rsfmri_mean_mask_path'], 
                                                        mean_subtraction_flag = data_parameters['mean_subtraction_flag'], 
                                                        scale_volumes_flag = data_parameters['scale_volumes_flag'], 
                                                        normalize_flag = data_parameters['normalize_flag'],
                                                        minus_one_scaling_flag = data_parameters['minus_one_scaling_flag'],
                                                        negative_flag = data_parameters['negative_flag'],
                                                        outlier_flag = data_parameters['outlier_flag'], 
                                                        shrinkage_flag = data_parameters['shrinkage_flag'], 
                                                        hard_shrinkage_flag = data_parameters['hard_shrinkage_flag'],
                                                        crop_flag = data_parameters['crop_flag']
                                                        )

    print("The lenght of the training inputs is {} and the size of the elements is {}".format(len(train_dMRI), train_dMRI[len(train_dMRI)-1].shape))
    print("The lenght of the training targets is {} and the size of the elements is {}".format(len(train_rsfMRI), train_dMRI[len(train_rsfMRI)-1].shape))

    write_hdf5(train_dMRI, train_rsfMRI, file_information, mode='train')

    del train_dMRI, train_rsfMRI                                                                                                 

    # Then, we'll do it for the validation data

    print('-> Processing validation data:')

    validation_dMRI, validation_rsfMRI = preprocessor.load_datasets(subjects = validation_subjects, 
                                                                    data_directory = data_parameters['data_directory'], 
                                                                    input_file = data_parameters['train_data_file'], 
                                                                    output_target = data_parameters['train_output_targets'], 
                                                                    mean_regression_flag = data_parameters['mean_regression_flag'], 
                                                                    mean_regression_all_flag = data_parameters['mean_regression_all_flag'], 
                                                                    regression_weights_path = data_parameters['regression_weights_path'],
                                                                    dMRI_mean_mask_path = data_parameters['dmri_mean_mask_path'], 
                                                                    rsfMRI_mean_mask_path = data_parameters['rsfmri_mean_mask_path'], 
                                                                    mean_subtraction_flag = data_parameters['mean_subtraction_flag'], 
                                                                    scale_volumes_flag = data_parameters['scale_volumes_flag'], 
                                                                    normalize_flag = data_parameters['normalize_flag'], 
                                                                    minus_one_scaling_flag = data_parameters['minus_one_scaling_flag'],
                                                                    negative_flag = data_parameters['negative_flag'],
                                                                    outlier_flag = data_parameters['outlier_flag'], 
                                                                    shrinkage_flag = data_parameters['shrinkage_flag'], 
                                                                    hard_shrinkage_flag = data_parameters['hard_shrinkage_flag'],
                                                                    crop_flag = data_parameters['crop_flag']
                                                                    )

    print("The lenght of the validation inputs is {} and the size of the elements is {}".format(len(validation_dMRI), validation_dMRI[len(validation_dMRI)-1].shape))
    print("The lenght of the validation targets is {} and the size of the elements is {}".format(len(validation_rsfMRI), validation_rsfMRI[len(validation_rsfMRI)-1].shape))

    write_hdf5(validation_dMRI, validation_rsfMRI, file_information, mode='validation')

    del validation_dMRI, validation_rsfMRI

def write_hdf5(input_volumes, target_volumes, file_information, mode):
    """ HDF5 Writer

    Function which writes the hdf5 files.

    Args:
        input_volumes (list): List of all the input volumes.
        target_volumes (list) List of all the target volumes.
        file_information (dict): Dictionary containing the outputs paths for the various databases
        mode (str): String indicating the type of data observed
    """

    if os.path.exists(file_information[mode]['input']):
        os.remove(file_information[mode]['input'])

    if os.path.exists(file_information[mode]['target']):
        os.remove(file_information[mode]['target'])

    with h5py.File(file_information[mode]['input'], 'w') as data_handle:
        data_handle.create_dataset('input', data=input_volumes)
    
    with h5py.File(file_information[mode]['target'], 'w') as data_handle:
        data_handle.create_dataset('target', data=target_volumes)


if __name__ == "__main__":

    print('Started Data Generation!')

    settings = Settings('utils/hdf5_settings.ini')
    data_parameters = settings['DATA']
    create_folder(data_parameters['data_folder_name'])

    file_information = {
                        'train': {"input" : os.path.join(data_parameters['data_folder_name'], data_parameters['input_data_train']),
                                  "target" : os.path.join(data_parameters['data_folder_name'], data_parameters['target_data_train']),
                                  },
                        'validation': {"input" : os.path.join(data_parameters['data_folder_name'], data_parameters['input_data_validation']),
                                       "target" : os.path.join(data_parameters['data_folder_name'], data_parameters['target_data_validation']),
                                      }
                        }

    convert_hdf5(data_parameters, file_information)

    print('Completed Data Generation!')

