"""Data Processing Functions

Description:

    This file contains the functions required for reading and loading the data into the network and preparing the various data files.

Usage:

    To use content from this folder, import the functions and instantiate them as you wish to use them:

        from utils.data_utils import function_name

"""

import os
import numpy as np
import torch
import torch.utils.data as data
import h5py
from fsl.data.image import Image
from fsl.utils.image.resample import resampleToPixdims
from fsl.utils.image.roi import roi

class DataMapper(data.Dataset):
    """Data Mapper Class

    This class represents a generic parent class for mapping between keys and data samples.
    The class represents a subclass/child class of data.Dataset, inheriting its functionality.
    This class is composed of a __init__ constructor, a __getitem__(), supporting fetching a data sample for a given key, and __len__(), which returns the size of the dataset.
    This class also has several other helper functions.

    Args:
        X (hdf5 Database): Database containing the input preprocessed volumes.
        y (hdf5 Database): Database containing the target preprocessed volumed.

    Returns:
        X_volume (torch.tensor): Tensor representation of the input data
        y_volume (torch.tensor): Tensor representation of the output data
        
    """

    def __init__(self, X, y):
        self.X = X
        self.y = y
        
    def __getitem__(self, index):
        X_volume = torch.from_numpy(self.X[index])
        y_volume = torch.from_numpy(self.y[index])

        return X_volume, y_volume

    def __len__(self):
        return len(self.y)


def get_datasets(data_parameters, cross_domain_x2x_flag, cross_domain_y2y_flag):
    """Data Loader Function.

    This function loads the various data file and returns the relevand mapped datasets.

    Args:
        data_parameters (dict): Dictionary containing relevant information for the datafiles.
        data_parameters = {
                            data_folder_name = "datasets"
                            input_data_train = "input_data_train.h5"
                            target_data_train = "target_data_train.h5"
                            input_data_validation = "input_data_validation.h5"
                            target_data_validation = "target_data_validation.h5"
                           }
        cross_domain_x2x_flag (bool): Flag indicating if cross-domain training is occuring between the inputs
        cross_domain_y2y_flag (bool): Flag indicating if cross-domain training is occuring between the targets

    Returns:
        touple: the relevant train and validation datasets
    """

    key_X = 'input'
    key_y = 'target'

    X_train_data = h5py.File(os.path.join(data_parameters["data_folder_name"], data_parameters["input_data_train"]), 'r')
    y_train_data = h5py.File(os.path.join(data_parameters["data_folder_name"], data_parameters["target_data_train"]), 'r')
    
    X_validation_data = h5py.File(os.path.join(data_parameters["data_folder_name"], data_parameters["input_data_validation"]), 'r')
    y_validation_data = h5py.File(os.path.join(data_parameters["data_folder_name"], data_parameters["target_data_validation"]), 'r')

    if cross_domain_x2x_flag == True:
        key_X = 'input'
        key_y = 'input'
    elif cross_domain_y2y_flag == True:
        key_X = 'target'
        key_y = 'target'
        
    return (
        DataMapper( X_train_data[key_X][()], y_train_data[key_y][()] ),
        DataMapper( X_validation_data[key_X][()], y_validation_data[key_y][()] )
    )


def load_file_paths(data_directory, data_list, mapping_data_file, mapping_targets_file=None):
    """File Loader

    This function returns a list of combined file paths for the input and output data.

    Args:
        data_directory (str): Path to input data directory
        data_list (str): Path to a .txt file containing the input files for consideration
        mapping_data_file (str): Path to the input files
        mapping_targets_file (str): Path to the target files

    Returns:
        file_paths (list): List containing the input data and target labelled output data
        volumes_to_be_used (list): List containing the volumes that will be used

    Raises:
        ValueError: "Invalid data entry - check code and data entry format!"
    """

    volumes_to_be_used = load_subjects_from_path(data_directory, data_list)

    if mapping_targets_file == None:
        file_paths = [[os.path.join(data_directory, volume, mapping_data_file)]
                      for volume in volumes_to_be_used]
    else:
        file_paths = [[os.path.join(data_directory, volume, mapping_data_file), 
                       os.path.join(data_directory, volume, mapping_targets_file)] for volume in volumes_to_be_used]

    return file_paths, volumes_to_be_used 


def load_subjects_from_path(data_directory, data_list):
    """ Text File Reader

    This function returns a list of combined file paths for the input and output data.

    Args:
        data_directory (str): Path to input data directory
        data_list (str): Path to a .txt file containing the input files for consideration

    Returns:
        volumes_to_be_used (list): List containing the volumes that will be used

    """

    if data_list:
        with open(data_list) as data_list_file:
            volumes_to_be_used = data_list_file.read().splitlines()
    else:
        volumes_to_be_used = [files for files in os.listdir(data_directory)]

    return volumes_to_be_used


def load_and_preprocess_evaluation(file_path, crop_flag, cross_domain_y2y_flag):
    """Load & Preprocessing before evaluation

    This function loads a nifty file and returns its volume and header information

    Args:
        file_path (str): Path to the desired file
        crop_flag (bool): Flag indicating if the volumes should be cropped from 91x109x91 to 72x90x77 to reduce storage space and speed-up training
        cross_domain_y2y_flag (bool): Flag indicating if cross-domain training is occuring between the targets

    Returns:
        volume (np.array): Array of training image data
        header (class): 'nibabel.nifti1.Nifti1Header' class object, containing image metadata
        xform (np.array): Array of shape (4, 4), containing the adjusted voxel-to-world transformation for the spatial dimensions of the resampled data

    Raises:
        ValueError: "Orientation value is invalid. It must be either >>coronal<<, >>axial<< or >>sagital<< "
    """

    original_image = Image(file_path[0])

    if cross_domain_y2y_flag == True:
        if crop_flag == False:
            volume = original_image.data[:, :, :, 0]
            header = Image(volume, header=original_image.header).header
        elif crop_flag == True:
            cropped = roi(original_image,((9,81),(10,100),(0,77)))
            volume = cropped.data[:, :, :, 0]
            header = cropped.header
        xform = None
    else:
        if crop_flag == False:
            volume, xform = resampleToPixdims(original_image, (2, 2, 2))
            header = Image(volume, header=original_image.header, xform=xform).header
        elif crop_flag == True:
            resampled, xform = resampleToPixdims(original_image, (2, 2, 2))
            resampled = Image(resampled, header=original_image.header, xform=xform)
            cropped = roi(resampled,((9,81),(10,100),(0,77)))
            volume = cropped.data
            header = cropped.header

    return volume, header, xform


def load_and_preprocess_targets(file_path, cross_domain_x2x_flag, crop_flag):
    """Load & Preprocessing targets before evaluation

    This function loads a nifty file and returns its volume information

    Args:
        file_path (str): Path to the desired file
        crop_flag (bool): Flag indicating if the volumes should be cropped from 91x109x91 to 72x90x77 to reduce storage space and speed-up training
        cross_domain_x2x_flag (bool): Flag indicating if cross-domain training is occuring between the inputs

    Returns:
        volume (np.array): Array of target image intensities.

   """

    original_image = Image(file_path[1])

    if cross_domain_x2x_flag == True:
        if crop_flag == False:
            volume, _ = resampleToPixdims(original_image, (2, 2, 2))
        elif crop_flag == True:
            resampled, xform = resampleToPixdims(original_image, (2, 2, 2))
            volume = roi(Image(resampled, header=original_image.header, xform=xform),((9,81),(10,100),(0,77))).data
    else:
        if crop_flag == False:
            volume = original_image.data[:, :, :, 0]
        elif crop_flag == True:
            volume = roi(original_image,((9,81),(10,100),(0,77))).data[:, :, :, 0]

    return volume


