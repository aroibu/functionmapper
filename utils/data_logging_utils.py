"""Data Logging Functions

Description:

    This folder contains several functions which, either on their own or included in larger pieces of software, perform data logging tasks.

Usage:

    To use content from this folder, import the functions and instantiate them as you wish to use them:

        from utils.data_logging_utils import function_name

"""

import os
import shutil
import logging
import numpy as np
import torch

# The SummaryWriter class provides a high-level API to create an event file in a given directory and add summaries and events to it.
# More here: https://tensorboardx.readthedocs.io/en/latest/tensorboard.html

from tensorboardX import SummaryWriter
import utils.data_evaluation_utils as evaluation


class LogWriter():

    """Log Writer class for the BrainMapper U-net.

    This class contains the pytorch implementation of the several logging functions required for the BrainMapper project.
    These functions are designed to keep track of progress during training, and also aid debugging.

    Args:
        number_of_classes (int): Number of classes
        logs_directory (str): Directory for outputing training logs
        experiment_name (str): Name of the experiment
        use_last_checkpoint (bool): Flag for loading the previous checkpoint
        labels (arr): Vector/Array of labels (if applicable)
        confusion_matrix_cmap (class): Colour Map to be used for the Conusion Matrix
    """

    def __init__(self, number_of_classes, logs_directory, experiment_name, use_last_checkpoint=False, labels=None):

        self.number_of_classes = number_of_classes
        training_logs_directory = os.path.join(
            logs_directory, experiment_name, "train")
        validation_logs_directory = os.path.join(
            logs_directory, experiment_name, "validation")

        # If the logs directory exist, we clear their contents to allow new logs to be created
        if not use_last_checkpoint:
            if os.path.exists(training_logs_directory):
                shutil.rmtree(training_logs_directory)
            if os.path.exists(validation_logs_directory):
                shutil.rmtree(validation_logs_directory)

        self.log_writer = {
            'train': SummaryWriter(logdir=training_logs_directory),
            'validation': SummaryWriter(logdir=validation_logs_directory)
        }

        self.current_iteration = 1

        self.labels = ['rsfMRI']

        self.logger = logging.getLogger()
        file_handler = logging.FileHandler(
            "{}/{}.log".format(os.path.join(logs_directory, experiment_name), "console_logs"))
        self.logger.addHandler(file_handler)

    def log(self, message):
        """Log function

        This function logs a message in the logger.

        Args:
            message (str): Message to be logged
        """

        self.logger.info(msg=message)

    def loss_per_iteration(self, loss_per_iteration, batch_index, iteration):
        """Log of loss / iteration

        This function records the loss for every iteration.

        Args:
            loss_per_iteration (torch.tensor): Value of loss for every iteration step
            batch_index (int): Index of current batch
            iteration (int): Current iteration value
        """

        print("Loss for Iteration {} is: {}".format(
            batch_index, loss_per_iteration))
        self.log_writer['train'].add_scalar(
            'loss/iteration', loss_per_iteration, iteration)

    def loss_per_epoch(self, losses, phase, epoch, previous_loss=None):
        """Log function

        This function records the loss for every epoch.

        Args:
            losses (list): Values of all the losses recorded during the training epoch
            phase (str): Current run mode or phase
            epoch (int): Current epoch value
            previous_loss(float): Value of the previous epoch's loss
        """

        loss = np.mean(losses)

        if phase == 'train':
            # loss = losses[-1]
            print("Loss for Epoch {} of {} is: {}".format(epoch, phase, loss))
        else:
            # loss = np.mean(losses)
            if previous_loss == None:
                print("Loss for Epoch {} of {} is: {}".format(epoch, phase, loss))
            else:
                print("Loss for Epoch {} of {} is {} and Absolute Change is {}".format(epoch, phase, loss, previous_loss - loss))

        self.log_writer[phase].add_scalar('loss/epoch', loss, epoch)


    def MSE_per_epoch(self, losses, phase, epoch, previous_loss=None):
        """Log function

        This function records the loss for every epoch.

        Args:
            losses (list): Values of all the losses recorded during the training epoch
            phase (str): Current run mode or phase
            epoch (int): Current epoch value
            previous_loss(float): Value of the previous epoch's loss
        """

        loss = np.mean(losses)

        if phase == 'train':
            # loss = losses[-1]
            print("MSE for Epoch {} of {} is: {}".format(epoch, phase, loss))
        else:
            # loss = np.mean(losses)
            if previous_loss == None:
                print("MSE for Epoch {} of {} is: {}".format(epoch, phase, loss))
            else:
                print("MSE for Epoch {} of {} is {} and Absolute Change is {}".format(epoch, phase, loss, previous_loss - loss))

        self.log_writer[phase].add_scalar('MSE/epoch', loss, epoch)

    def close(self):
        """Close the log writer

        This function closes the two log writers.
        """

        self.log_writer['train'].close()
        self.log_writer['validation'].close()

    def add_graph(self, model):
        """Produces network graph

        This function produces the network graph

        NOTE: Currently, the function suffers from bugs and is not implemented.

        Args:
            model (torch.nn.Module): Model to draw.
        """

        self.log_writer['train'].add_graph(model)
