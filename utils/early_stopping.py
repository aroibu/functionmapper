"""Early Stopping Class

Description:

    This file contains a class which is used to early stop the training of the network to avoid overfitting on the training dataset.

Usage:

    To use content from this function, import the functions and instantiate them as you wish to use them:

        from utils.early_stopping import EarlyStopping
        early_stop_check = EarlyStopping(parameters)
        early_stop = early_stop_check(parameters)

"""

import torch
import numpy as np

class EarlyStopping:
    """Early Stopping class

    This class is used as a form of regularization used to avoid overfitting on the training dataset. 
    Early stopping keeps track of the validation loss.
    If the loss stops decreasing for several epochs (represented as patience) in a row the training stop signal is transmitted.

    Args:
        patience (int): Metric for keeping track of the number of consecutive epochs the validation loss is allowed to decrease.
        min_delta (int): Minimum change in the monitored quantity to quality as an improvement (ie. if absolute change is less than min_delta, it will count as no improvement)

    Returns:
        early_stop (bool): Flag indicating if the training should be terminated
        save_checkpoint (bool): Flag indicating if the checkpoint should be saved

    """

    def __init__(self, patience=5, min_delta=0.0, best_score=None, counter=0):

        self.patience = patience
        self.counter = counter
        self.best_score = best_score
        self.early_stop = False
        self.min_delta = min_delta

    def __call__(self, validation_loss): 

        score = validation_loss

        if self.best_score is None:
            self.best_score = score
        
        elif np.greater_equal(self.min_delta, self.best_score - score):
            self.counter += 1
            print("Early Stopping Counter: {}/{}".format(self.counter, self.patience))
            if self.counter >= self.patience:
                self.early_stop = True

        else:
            self.best_score = score
            self.counter = 0

        return self.early_stop, self.best_score, self.counter