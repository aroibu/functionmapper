"""Brain Mapper U-Net Architecture

Description:

    This folder contains the Pytorch implementation of the core U-net architecture.
    This arcitecture predicts functional connectivity rsfMRI from structural connectivity information from dMRI.

Usage:

    To use this module, import it and instantiate is as you wish:

        from BrainMapperUNet import BrainMapperUNet
        deep_learning_model = BrainMapperUnet(parameters)

"""

import numpy as np
import torch
import torch.nn as nn
import utils.modules as modules
from torch.nn.init import _calculate_fan_in_and_fan_out as calculate_fan


class BrainMapperAE3D(nn.Module):
    """Architecture class for CycleGAN inspired BrainMapper 3D Autoencoder.

    This class contains the pytorch implementation of the generator architecture underpinning the BrainMapper project.

    Args:
        parameters (dict): Contains information relevant parameters
        parameters = {
            'kernel_size': 5
            'kernel_width': 5
            'kernel_depth': 5
            'kernel_classification': 1
            'input_channels': 1
            'output_channels': 64
            'convolution_stride': 1
            'dropout': 0.2
            'pool_kernel_size': 2
            'pool_stride': 2
            'up_mode': 'upconv'
            'number_of_classes': 1
        }

    Returns:
        probability_map (torch.tensor): Output forward passed tensor through the U-net block
    """

    def __init__(self, parameters):
        super(BrainMapperAE3D, self).__init__()

        self.cross_domain_x2y_flag = parameters['cross_domain_x2y_flag']

        original_input_channels = parameters['input_channels']
        original_output_channels = parameters['output_channels']
        original_kernel_size = parameters['kernel_size']
        original_stride = parameters['convolution_stride']

        # Encoder Path

        parameters['kernel_size'] = parameters['first_kernel_size']
        parameters['convolution_stride'] = parameters['first_convolution_stride']
        self.encoderBlocks = nn.ModuleList([modules.ResNetEncoderBlock3D(parameters)])

        parameters['kernel_size'] = original_kernel_size
        parameters['convolution_stride'] = original_stride

        equal_channels_blocks = 0

        for _ in range(parameters['number_of_encoder_blocks']):
            if parameters['output_channels'] < parameters['max_number_channels']:
                parameters['input_channels'] = parameters['output_channels']
                parameters['output_channels'] = parameters['output_channels'] * 2
            else:
                parameters['input_channels'] = parameters['output_channels']
                equal_channels_blocks += 1 
            self.encoderBlocks.append(modules.ResNetEncoderBlock3D(parameters))

        # Transformer

        parameters['input_channels'] = parameters['output_channels']
        parameters['convolution_stride'] = parameters['transformer_blocks_stride']

        self.transformerBlocks = nn.ModuleList([modules.ResNetBlock3D(parameters) for i in range(parameters['number_of_transformer_blocks'])])

        if self.cross_domain_x2y_flag == True:
            self.featureMappingLayers = nn.ModuleList([modules.ResNetFeatureMappingBlock3D(parameters) for i in range(parameters['number_of_feature_mapping_blocks'])])

        # Decoder

        if equal_channels_blocks != 0:
            self.decoderBlocks = nn.ModuleList([modules.ResNetDecoderBlock3D(parameters) for i in range(equal_channels_blocks)])

        parameters['output_channels'] = parameters['output_channels'] // 2
        if equal_channels_blocks != 0:
            self.decoderBlocks.append(modules.ResNetDecoderBlock3D(parameters))
        else:
            self.decoderBlocks = nn.ModuleList([modules.ResNetDecoderBlock3D(parameters)])

        for _ in range(parameters['number_of_encoder_blocks'] - equal_channels_blocks - 1):
            parameters['input_channels'] = parameters['output_channels']
            parameters['output_channels'] = parameters['output_channels'] // 2
            self.decoderBlocks.append(modules.ResNetDecoderBlock3D(parameters))

        parameters['input_channels'] = parameters['output_channels']
        self.decoderBlocks.append(modules.ResNetClassifierBlock3D(parameters))

        parameters['input_channels'] = original_input_channels
        parameters['output_channels'] = original_output_channels

    def forward(self, X):
        """Forward pass for 3D CGAN Autoencoder

        Function computing the forward pass through the 3D generator
        The input to the function is the dMRI map

        Args:
            X (torch.tensor): Input dMRI map, shape = (N x C x D x H x W) 

        Returns:
            probability_map (torch.tensor): Output forward passed tensor through the CGAN Autoencoder
        """

        # Encoder

        Y_encoder_sizes = []

        for encoderBlock in self.encoderBlocks:
            X = encoderBlock.forward(X)
            Y_encoder_sizes.append(X.size())

        Y_encoder_sizes = Y_encoder_sizes[:-1][::-1]
        Y_encoder_sizes_lenght = len(Y_encoder_sizes)

        # Transformer

        if self.cross_domain_x2y_flag == True:
            for transformerBlock in self.transformerBlocks[:len(self.transformerBlocks)//2]:
                X = transformerBlock(X)

            for featureMappingLayer in self.featureMappingLayers:
                X = featureMappingLayer(X)

            for transformerBlock in self.transformerBlocks[len(self.transformerBlocks)//2:]:
                X = transformerBlock(X)

        else:
            for transformerBlock in self.transformerBlocks:
                X = transformerBlock(X)

        # Decoder

        for index, decoderBlock in enumerate(self.decoderBlocks):
            if index < Y_encoder_sizes_lenght:
                X = decoderBlock.forward(X, Y_encoder_sizes[index])
            else:
                X = decoderBlock.forward(X)

        del Y_encoder_sizes, Y_encoder_sizes_lenght

        return X

    def save(self, path):
        """Model Saver

        Function saving the model with all its parameters to a given path.
        The path must end with a *.model argument.

        Args:
            path (str): Path string
        """

        print("Saving Model... {}".format(path))
        torch.save(self, path)

    @property
    def test_if_cuda(self):
        """Cuda Test

        This function tests if the model parameters are allocated to a CUDA enabled GPU.

        Returns:
            bool: Flag indicating True if the tensor is stored on the GPU and Flase otherwhise
        """

        return next(self.parameters()).is_cuda

    def predict(self, X, device=0):
        """Post-training Output Prediction

        This function predicts the output of the of the U-net post-training

        Args:
            X (torch.tensor): input dMRI volume
            device (int/str): Device type used for training (int - GPU id, str- CPU)

        Returns:
            prediction (ndarray): predicted output after training

        """
        self.eval()  # PyToch module setting network to evaluation mode

        if type(X) is np.ndarray:
            X = torch.tensor(X, requires_grad=False).type(torch.FloatTensor)
        elif type(X) is torch.Tensor and not X.is_cuda:
            X = X.type(torch.FloatTensor).cuda(device, non_blocking=True)

        # .cuda() call transfers the densor from the CPU to the GPU if that is the case.
        # Non-blocking argument lets the caller bypas synchronization when necessary

        with torch.no_grad():  # Causes operations to have no gradients
            output = self.forward(X)

        _, idx = torch.max(output, 1)

        # We retrieve the tensor held by idx (.data), and map it to a cpu as an ndarray
        idx = idx.data.cpu().numpy()

        prediction = np.squeeze(idx)

        del X, output, idx

        return prediction

    def reset_parameters(self, custom_weight_reset_flag):
        """Parameter Initialization

        This function (re)initializes the parameters of the defined network.
        This function is a wrapper for the reset_parameters() function defined for each module. 
        More information can be found here: https://discuss.pytorch.org/t/what-is-the-default-initialization-of-a-conv2d-layer-and-linear-layer/16055 + https://discuss.pytorch.org/t/how-to-reset-model-weights-to-effectively-implement-crossvalidation/53859 
        An alternative (re)initialization method is described here: https://discuss.pytorch.org/t/how-to-reset-variables-values-in-nn-modules/32639 

        Args:
            custom_weight_reset_flag (bool): Flag indicating if the modified weight initialisation approach should be used. 

        """

        print("Initializing network parameters...")

        for _, module in self.named_children():
            for _, submodule in module.named_children():
                if isinstance(submodule, (torch.nn.ConvTranspose3d, torch.nn.Conv3d, torch.nn.InstanceNorm3d)) == True:
                    submodule.reset_parameters()
                    if custom_weight_reset_flag == True:
                        if isinstance(submodule, (torch.nn.Conv3d, torch.nn.ConvTranspose3d)):                        
                            gain = np.sqrt(np.divide(2, 1 + np.power(0.25, 2)))
                            fan, _ = calculate_fan(submodule.weight)
                            std = np.divide(gain, np.sqrt(fan))
                            submodule.weight.data.normal_(0, std)
                for _, subsubmodule in submodule.named_children():
                    if isinstance(subsubmodule, (torch.nn.ConvTranspose3d, torch.nn.Conv3d, torch.nn.InstanceNorm3d)) == True:
                        subsubmodule.reset_parameters()
                        if custom_weight_reset_flag == True:
                            if isinstance(subsubmodule, (torch.nn.Conv3d, torch.nn.ConvTranspose3d)):
                                gain = np.sqrt(np.divide(2, 1 + np.power(0.25, 2)))
                                fan, _ = calculate_fan(subsubmodule.weight)
                                std = np.divide(gain, np.sqrt(fan))
                                subsubmodule.weight.data.normal_(0, std)

        print("Initialized network parameters!")


class AutoEncoder3D(nn.Module):
    """Architecture class for CycleGAN inspired BrainMapper 3D Autoencoder.

    This class contains the pytorch implementation of the generator architecture underpinning the BrainMapper project.

    Args:
        parameters (dict): Contains information relevant parameters
        parameters = {
            'kernel_size': 5
            'kernel_width': 5
            'kernel_depth': 5
            'kernel_classification': 1
            'input_channels': 1
            'output_channels': 64
            'convolution_stride': 1
            'dropout': 0.2
            'pool_kernel_size': 2
            'pool_stride': 2
            'up_mode': 'upconv'
            'number_of_classes': 1
        }

    Returns:
        probability_map (torch.tensor): Output forward passed tensor through the U-net block
    """

    def __init__(self, parameters):
        super(AutoEncoder3D, self).__init__()

        self.cross_domain_x2y_flag = parameters['cross_domain_x2y_flag']

        original_input_channels = parameters['input_channels']
        original_output_channels = parameters['output_channels']
        original_kernel_size = parameters['kernel_size']
        original_stride = parameters['convolution_stride']

        # Encoder Path

        parameters['kernel_size'] = parameters['first_kernel_size']
        parameters['convolution_stride'] = parameters['first_convolution_stride']
        self.encoderBlocks = nn.ModuleList([modules.ResNetEncoderBlock3D(parameters)])

        parameters['kernel_size'] = original_kernel_size
        parameters['convolution_stride'] = original_stride

        equal_channels_blocks = 0

        for _ in range(parameters['number_of_encoder_blocks']):
            if parameters['output_channels'] < parameters['max_number_channels']:
                parameters['input_channels'] = parameters['output_channels']
                parameters['output_channels'] = parameters['output_channels'] * 2
            else:
                parameters['input_channels'] = parameters['output_channels']
                equal_channels_blocks += 1 
            self.encoderBlocks.append(modules.ResNetEncoderBlock3D(parameters))

        # Decoder

        parameters['input_channels'] = parameters['output_channels']
        parameters['convolution_stride'] = parameters['transformer_blocks_stride']

        if equal_channels_blocks != 0:
            self.decoderBlocks = nn.ModuleList([modules.ResNetDecoderBlock3D(parameters) for i in range(equal_channels_blocks)])

        parameters['output_channels'] = parameters['output_channels'] // 2
        if equal_channels_blocks != 0:
            self.decoderBlocks.append(modules.ResNetDecoderBlock3D(parameters))
        else:
            self.decoderBlocks = nn.ModuleList([modules.ResNetDecoderBlock3D(parameters)])

        for _ in range(parameters['number_of_encoder_blocks'] - equal_channels_blocks - 1):
            parameters['input_channels'] = parameters['output_channels']
            parameters['output_channels'] = parameters['output_channels'] // 2
            self.decoderBlocks.append(modules.ResNetDecoderBlock3D(parameters))

        parameters['input_channels'] = parameters['output_channels']
        self.decoderBlocks.append(modules.ResNetClassifierBlock3D(parameters))

        parameters['input_channels'] = original_input_channels
        parameters['output_channels'] = original_output_channels

    def forward(self, X):
        """Forward pass for 3D CGAN Autoencoder

        Function computing the forward pass through the 3D generator
        The input to the function is the dMRI map

        Args:
            X (torch.tensor): Input dMRI map, shape = (N x C x D x H x W) 

        Returns:
            probability_map (torch.tensor): Output forward passed tensor through the CGAN Autoencoder
        """

        # Encoder

        Y_encoder_sizes = []

        for encoderBlock in self.encoderBlocks:
            X = encoderBlock.forward(X)
            Y_encoder_sizes.append(X.size())

        Y_encoder_sizes = Y_encoder_sizes[:-1][::-1]
        Y_encoder_sizes_lenght = len(Y_encoder_sizes)

        # Decoder

        for index, decoderBlock in enumerate(self.decoderBlocks):
            if index < Y_encoder_sizes_lenght:
                X = decoderBlock.forward(X, Y_encoder_sizes[index])
            else:
                X = decoderBlock.forward(X)

        del Y_encoder_sizes, Y_encoder_sizes_lenght

        return X

    def save(self, path):
        """Model Saver

        Function saving the model with all its parameters to a given path.
        The path must end with a *.model argument.

        Args:
            path (str): Path string
        """

        print("Saving Model... {}".format(path))
        torch.save(self, path)

    @property
    def test_if_cuda(self):
        """Cuda Test

        This function tests if the model parameters are allocated to a CUDA enabled GPU.

        Returns:
            bool: Flag indicating True if the tensor is stored on the GPU and Flase otherwhise
        """

        return next(self.parameters()).is_cuda

    def predict(self, X, device=0):
        """Post-training Output Prediction

        This function predicts the output of the of the U-net post-training

        Args:
            X (torch.tensor): input dMRI volume
            device (int/str): Device type used for training (int - GPU id, str- CPU)

        Returns:
            prediction (ndarray): predicted output after training

        """
        self.eval()  # PyToch module setting network to evaluation mode

        if type(X) is np.ndarray:
            X = torch.tensor(X, requires_grad=False).type(torch.FloatTensor)
        elif type(X) is torch.Tensor and not X.is_cuda:
            X = X.type(torch.FloatTensor).cuda(device, non_blocking=True)

        # .cuda() call transfers the densor from the CPU to the GPU if that is the case.
        # Non-blocking argument lets the caller bypas synchronization when necessary

        with torch.no_grad():  # Causes operations to have no gradients
            output = self.forward(X)

        _, idx = torch.max(output, 1)

        # We retrieve the tensor held by idx (.data), and map it to a cpu as an ndarray
        idx = idx.data.cpu().numpy()

        prediction = np.squeeze(idx)

        del X, output, idx

        return prediction

    def reset_parameters(self, custom_weight_reset_flag):
        """Parameter Initialization

        This function (re)initializes the parameters of the defined network.
        This function is a wrapper for the reset_parameters() function defined for each module. 
        More information can be found here: https://discuss.pytorch.org/t/what-is-the-default-initialization-of-a-conv2d-layer-and-linear-layer/16055 + https://discuss.pytorch.org/t/how-to-reset-model-weights-to-effectively-implement-crossvalidation/53859 
        An alternative (re)initialization method is described here: https://discuss.pytorch.org/t/how-to-reset-variables-values-in-nn-modules/32639 

        Args:
            custom_weight_reset_flag (bool): Flag indicating if the modified weight initialisation approach should be used. 

        """

        print("Initializing network parameters...")

        for _, module in self.named_children():
            for _, submodule in module.named_children():
                if isinstance(submodule, (torch.nn.ConvTranspose3d, torch.nn.Conv3d, torch.nn.InstanceNorm3d)) == True:
                    submodule.reset_parameters()
                    if custom_weight_reset_flag == True:
                        if isinstance(submodule, (torch.nn.Conv3d, torch.nn.ConvTranspose3d)):                        
                            gain = np.sqrt(np.divide(2, 1 + np.power(0.25, 2)))
                            fan, _ = calculate_fan(submodule.weight)
                            std = np.divide(gain, np.sqrt(fan))
                            submodule.weight.data.normal_(0, std)
                for _, subsubmodule in submodule.named_children():
                    if isinstance(subsubmodule, (torch.nn.ConvTranspose3d, torch.nn.Conv3d, torch.nn.InstanceNorm3d)) == True:
                        subsubmodule.reset_parameters()
                        if custom_weight_reset_flag == True:
                            if isinstance(subsubmodule, (torch.nn.Conv3d, torch.nn.ConvTranspose3d)):
                                gain = np.sqrt(np.divide(2, 1 + np.power(0.25, 2)))
                                fan, _ = calculate_fan(subsubmodule.weight)
                                std = np.divide(gain, np.sqrt(fan))
                                subsubmodule.weight.data.normal_(0, std)

        print("Initialized network parameters!")